//Bradley Halbert
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
 char **readhomes(char *filename)
    {
        int size = 50;
        char **homes = (char **)malloc(size* sizeof(char *));

        char str[40];
        int i = 0;

        FILE *fp = fopen(filename, "r");
        //while(fscanf(fp, "%d,%s,%d,%d", int zip, str, int price, int number) != EOF)
        while(fscanf(fp, "%[^\n]\n", str) != EOF)
        {
            if(i == size)
            {
                size += 10;
                char **newarr = (char **)realloc(homes, size * sizeof(char *));
                if(newarr != NULL) homes = newarr;
                else
                {
                    printf("Ralloc failed]n");
                    exit(1);
                }
            }
            char *newstr = (char *)malloc(strlen(str) + 1 * sizeof(char));
            strcpy(newstr, str);
            homes[i] = newstr;

            i++;
        }
        fclose(fp);
        return homes;
    }

int home_count(char **homes)
{
    int count = 0;
    for(int i = 0; homes[i] != NULL; i++)
    {
        count++;
    }
    return count;

}

int zip_count(char *zipcode, char **homes)
{
    int count = 0;
    for(int i = 0; homes[i] != NULL; i++)
    {
        char *token;
        char temp[40];
        int compare = 0;
        strcpy(temp, homes[i]);
        token = strtok(temp, ",");
        compare = strcmp(token, zipcode);
        if (compare == 0)
        {
            count++;
        }

    }
    return count;
}

int price_count(char *userprice, char **homes)
{
    
    printf("\n");
    printf("The following are the houses in that price range. \n");
    printf("\n");
    for(int i = 0; homes[i] != NULL; i++)
    {
        char *zipcode;
        char *address;
        char *price;
        char *number;
        char *token;
        char temp[20];

        strcpy(temp, homes[i]);

        zipcode = strtok(temp, ",");
        address = strtok(NULL, ",");
        price = strtok(NULL, ",");
        number = strtok(NULL, ",");

        int newprice = atoi(price);
        int usersprice = atoi(userprice);
        if((newprice >= usersprice && newprice <= usersprice + 10000) || (newprice <= usersprice && newprice >= usersprice - 10000))
        {
            printf("ADDRESS: %s PRICE: %d\n", address, newprice);
        }
    }
}

int main(int argc, char *argv[])
{
    // TODO: Use readhomes to get the array of structs
    char **homes = readhomes("listings.csv");


    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.

    int homecount = home_count(homes);
    printf("\n");
    printf("There are a total of %d homes.\n", homecount);

    
    // TODO: Prompt the user to type in a zip code.
    char zip[8];
    printf("\n");
    printf("Please enter a zip code: ");
    scanf("%s", &zip);

    
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    
    int number = zip_count(zip, homes);
    printf("There are %d homes with that zip code\n", number);
    

    // TODO: Prompt the user to type in a price.
    char price[8];
    printf("\n");
    printf("Please enter a house price: ");
    scanf("%s", &price);
    

    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    price_count(price, homes);


}
 