// Define a struct to represent a single home

typedef struct home
{
	int zip;
	char *addr;
	int price;
	int area;
} home;
